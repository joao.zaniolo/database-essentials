import { Router } from 'express';

import User from './schemas/User.js';

const router = Router();

router.post('/', async (request, response) => {
  const { email, password } = request.body;

  const user = new User({ email, password });

  try {
    const savedUser = await user.save();

    return response.status(201).json(savedUser);
  } catch (error) {
    return response.status(500).json({ message: error });
  }
});

router.get('/', async (_, response) => {
  try {
    const users = await User.find();

    return response.status(200).json(users);
  } catch (err) {
    return response.status(500).json({ message: err });
  }
});

router.get('/:_id', async (request, response) => {
  try {
    const { _id } = request.params;
    const user = await User.findOne({ _id });

    return response.status(200).json(user);
  } catch (err) {
    return response.status(404).json({ message: err });
  }
});

router.put('/:_id', async (request, response) => {
  try {
    const { _id } = request.params;
    const user = await User.findOneAndUpdate({ _id }, request.body, {
      new: true,
    });

    return response.status(200).json(user);
  } catch (err) {
    return response.status(404).json({ message: err });
  }
});

router.delete('/:_id', async (request, response) => {
  try {
    const { _id } = request.params;
    await User.findOneAndDelete({ _id });

    return response.sendStatus(200);
  } catch (err) {
    return response.status(404).json({ message: err });
  }
});

export default router;
