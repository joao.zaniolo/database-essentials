import mongoose from 'mongoose';

export default mongoose.model(
  'Users',
  mongoose.Schema(
    {
      email: {
        type: String,
        required: true,
      },
      password: {
        type: String,
        required: true,
        select: false,
      },
    },
    {
      timestamps: true,
    }
  )
);
