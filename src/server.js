import 'dotenv/config.js';

import express from 'express';
import mongoose from 'mongoose';

import routes from './routes/index.js';

const app = express();

app.use(express.json());
app.use(routes);

mongoose.connect(
  process.env.MONGO_CONNECTION,
  { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false },
  () => {
    console.log('🌲 Connected to MongoDB');
  }
);

app.listen('3333', () => {
  console.log('🚀 Server running at http://localhost:3333');
});
